# GiftAssignment

Given a CSV File of names, shuffle the list of names avoiding self- and circular- (with 4+ people) gifting.

## Getting Started

I built and ran this in JetBrains Rider. VS Code (with extensions) or VS Community should work as well.

### Prerequisites

* [.NET 5.0](https://www.microsoft.com/net/download)
* Operating Systems 
    * Linux (tested on Ubuntu 20.10, PopOS!)
    * Windows (tested on 10)
* (optional) [GraphViz](https://graphviz.org/download)
    * Ubuntu can install it using `sudo apt install graphviz`
    * Windows has multiple installation options. Just make sure it gets added to the PATH environment variable.

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details
