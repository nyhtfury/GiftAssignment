﻿#nullable enable
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;

namespace GiftAssignment
{
    internal static class Program
    {
        private const string DotFileName = "giftAssignments.dot";
        private const string ImageFileName = "giftAssignment.png";
        
        private static void Main()
        {
            Console.Write("Please Enter a File Path to a .csv file containing gifter names: ");
            var filepath = Console.ReadLine();
            if (string.IsNullOrWhiteSpace(filepath))
            {
                filepath = Directory.GetFiles(Environment.CurrentDirectory).First(x => x.EndsWith(".csv"));
            }
            
            var giftGivers = GiftFunctions.ReadGiftGiverList(filepath);

            switch (giftGivers.Count)
            {
                case 0:
                    Console.WriteLine("No gift givers. Must not be in the mood for gifting.");
                    return;
                case 1:
                    Console.WriteLine("There's only one person giving & receiving gifts... I think you can figure that out.");
                    return;
            }

            var giftRecievers = new List<string>(giftGivers);
            var loopCount = 0;

            if (giftGivers.Count == 2)
            {
                giftRecievers.Reverse();
                
                Console.WriteLine("With only two people, no iterations were required.");
            }
            else
            {
                do
                {
                    giftRecievers = giftRecievers.Shuffle();
                    loopCount++;
                } while (GiftFunctions.CircularGiftGiversPresent(giftGivers, giftRecievers)); 
                
                Console.WriteLine($"After {loopCount} iterations... No circular or self gift giving.");
            }

            var giftAssignments = giftGivers.Zip(giftRecievers, (g, r) => $"\"{g}\" -> \"{r}\"").ToArray();
            var digraph = "digraph {\n\t" + string.Join("\n\t", giftAssignments) + "\n}";

            var dotFilePath = Path.Combine(Environment.CurrentDirectory + Path.DirectorySeparatorChar + DotFileName);

            try
            {
                if (File.Exists(dotFilePath)) File.Delete(dotFilePath);
                using var sw = new StreamWriter(dotFilePath);
                sw.WriteLine(digraph);
                sw.Close();
                
                Console.WriteLine($"Created a dot (text) file with gift assignments: {dotFilePath}");
            }
            catch (Exception e)
            {
                e.ConsoleLog($"Deleting the existing or writing to the dot file ({dotFilePath}) failed.");
            }

            var graphVizSuccess = false;
            var imageFilePath =
                Path.Combine(Environment.CurrentDirectory + Path.DirectorySeparatorChar + ImageFileName);
            
            try
            {
                if (File.Exists(imageFilePath)) File.Delete(imageFilePath);
                
                if (OperatingSystem.IsLinux())
                {
                    var graphviz = new Process
                    {
                        StartInfo = new ProcessStartInfo
                        {
                            // ReSharper disable once StringLiteralTypo
                            ArgumentList = {"-Tpng", dotFilePath, "-o", ImageFileName},
                            CreateNoWindow = true,
                            FileName = "dot"
                        }
                    };
                    
                    graphviz.Start();
                    graphviz.WaitForExit();
                }
                else if (OperatingSystem.IsWindows())
                {
                    // dot needs to be part of the PATH Environment Variable
                    RunWindowsCommand($"dot.exe -Tpng {dotFilePath} -o {ImageFileName}");
                }
                
                Console.WriteLine($"Generated GraphViz image: {imageFilePath}");
                graphVizSuccess = true;
            }
            catch (Exception e)
            {
                e.ConsoleLog(
                    "Failed to run graphviz. It may not be installed on this system, or not part of the path.");
            }

            if (!graphVizSuccess) return;
            try
            {
                Console.WriteLine("Opening the generated GraphViz image...");
                if (OperatingSystem.IsLinux())
                {
                    Console.Write("Would you like to set a new default for mimeopen (Y/N): ");
                    var response = Console.ReadLine();
                    if (!string.IsNullOrWhiteSpace(response) && (response.StartsWith('y') || response.StartsWith('Y')))
                    {
                        RunLinuxCommand($"mimeopen -d {imageFilePath}");
                    }
                    else
                    {
                        RunLinuxCommand($"mimeopen {imageFilePath}");    
                    }
                }
                else if (OperatingSystem.IsWindows())
                {
                    RunWindowsCommand($"\"{imageFilePath}\"");
                }
            }
            catch (Exception e)
            {
                e.ConsoleLog("Failed to open the image generated by graphviz.");
            }
        }
        
        /// <summary>
        /// (Only on Linux, does nothing elsewhere)
        /// Run bash command. Automatically escapes double quotes.
        /// </summary>
        /// <param name="cmd">command to run using bash on linux</param>
        /// <param name="path">(optional) specify location of bash, or another command entirely</param>
        private static void RunLinuxCommand(string cmd, string path = "/bin/bash")
        {
            if (!OperatingSystem.IsLinux()) return;
            var escapedArgs = cmd.Replace("\"", "\\\"");
        
            using var process = new Process
            {
                StartInfo = new ProcessStartInfo
                {
                    RedirectStandardOutput = true,
                    RedirectStandardError = true,
                    UseShellExecute = false,
                    CreateNoWindow = true,
                    WindowStyle = ProcessWindowStyle.Hidden,
                    FileName = path,
                    Arguments = $"-c \"{escapedArgs}\""
                }
            };

            process.OutputDataReceived += (sender, args) =>
            {
                if (!string.IsNullOrWhiteSpace(args.Data))
                {
                    Console.WriteLine(args.Data);
                }
            };
            process.ErrorDataReceived += (sender, args) =>
            {
                if (!string.IsNullOrWhiteSpace(args.Data))
                {
                    Console.WriteLine(args.Data);
                }
            };
            process.EnableRaisingEvents = true;
            process.Start();
            process.BeginOutputReadLine();
            process.BeginErrorReadLine();
            process.WaitForExit();
        }
        
        /// <summary>
        /// (Only on Windows, does nothing elsewhere)
        /// Run cmd.exe command.
        /// </summary>
        /// <param name="cmd">command to run using cmd on Windows</param>
        /// <param name="path">(optional) specify location of cmd.exe, or another command entirely</param>
        private static void RunWindowsCommand(string cmd, string path = "cmd.exe")
        {
            if (!OperatingSystem.IsWindows()) return;
        
            using var process = new Process
            {
                StartInfo = new ProcessStartInfo
                {
                    WindowStyle = ProcessWindowStyle.Hidden,
                    FileName = path,
                    Arguments = $"/C \"{cmd}\"" // /C tells CMD to terminate immediately
                }
            };

            process.Start();
            process.WaitForExit();
        }
    }

    public static class GiftFunctions
    {
        /// <summary>
        ///     Given two lists, see if any of their indices match or if two people are giving each other gifts.
        ///     Throw an exception if lists are different lengths.
        /// </summary>
        /// <param name="giftGivers">List of strings (Names) of people giving gifts</param>
        /// <param name="giftRecievers">Same List of strings (Names) of people receiving gifts.</param>
        /// <returns>If none of the indices match, true. False otherwise.</returns>
        public static bool CircularGiftGiversPresent(IList<string> giftGivers, IList<string> giftRecievers)
        {
            if (!giftGivers.SameLengthAs(giftRecievers))
                throw new Exception("List Length Mismatch in CircularGiftGiversPresent");
            return giftGivers.Where((t, i) => t == giftRecievers[giftGivers.IndexOf(giftRecievers[i])]).Any();
        }

        // ReSharper disable twice InvalidXmlDocComment
        /// <summary>
        ///     Read a CSV File. Split lines up by '\r' then further split lines by ',' into individual entries.
        /// </summary>
        /// <param name="filePath">Path to the CSV File. Works with local paths.</param>
        /// <returns>Return a List<string> of all found items in CSV.</returns>
        public static List<string> ReadGiftGiverList(string filePath)
        {
            List<string> giftGivers = new();
            var lines = File.ReadLines(filePath).Select(a => a.Split('\r'));
            foreach (var line in lines)
            foreach (var entry in line)
                giftGivers.AddRange(entry.Split(',').Where(s => !string.IsNullOrWhiteSpace(s)));
            return giftGivers;
        }
    }

    /// <summary>
    /// Get random threads safely and track the one we use the first time.
    /// </summary>
    public static class ThreadSafeRandom
    {
        [ThreadStatic] private static Random? _local;

        /// <summary>
        /// Get a new random thread (Safely)
        /// </summary>
        public static Random ThisThreadsRandom
        {
            get
            {
                return _local ??=
                    new Random(unchecked(Environment.TickCount * 31 + Thread.CurrentThread.ManagedThreadId));
            }
        }
    }

    internal static class Extensions
    {
        /// <summary>
        /// Shuffle a list using a random tick seed.
        /// </summary>
        /// <param name="list">the list to shuffle</param>
        /// <typeparam name="T">the type of data in the list</typeparam>
        /// <returns>a list that has been reorganized/shuffled</returns>
        public static List<T> Shuffle<T>(this List<T> list)
        {
            var n = list.Count;
            while (n > 1)
            {
                n--;
                var k = ThreadSafeRandom.ThisThreadsRandom.Next(n + 1);
                var value = list[k];
                list[k] = list[n];
                list[n] = value;
            }

            return list;
        }

        /// <summary>
        /// Check if two ILists of the same type are the same length
        /// </summary>
        /// <returns>true if ILists are same length, false otherwise</returns>
        public static bool SameLengthAs<T>(this IList<T> thisEnumerable, IList<T> anotherEnumerable) =>
            thisEnumerable.Count == anotherEnumerable.Count;

        /// <summary>
        /// Write a message, exception message, and exception stack trace to the console.
        /// </summary>
        /// <param name="e">exception whose message and stack trace will be written to the console</param>
        /// <param name="customMsg">custom message to write to the console</param>
        public static void ConsoleLog(this Exception e, string customMsg)
        {
            Console.WriteLine($"{customMsg}\n\n");
            Console.WriteLine(e.Message + "\n" + e.StackTrace);
        }
    }
}